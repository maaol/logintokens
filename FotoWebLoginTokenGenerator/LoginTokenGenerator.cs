// LoginTokenGenerator.cs - Example code for generating FotoWeb login tokens
// (C) 1995-2015 FotoWare a.s.
// See LICENSE.txt for terms of use and distribution

using System;
using System.IO;
using System.Security.Cryptography;

namespace FotoWareNet.FotoWeb
{
	/// <summary>
	/// Helper class for creating and parsing login tokens.
    /// Usage:
    /// 
    /// LoginTokenGenerator g = new LoginTokenGenerator(encryptionSecret);
    /// 
    /// string token = g.CreateLoginToken(userName);
    /// 
    /// if(!g.DecodeLoginToken(token, userName, remainingTime)) {
    ///   // invalid token
    /// }
    /// 
    /// Format of a login token:
    /// s=START_TIME;e=END_TIME;w=0|1;u=USERNAME
    /// where:
    /// - START_TIME: Start time of validity of the token in "yyyy-MM-dd HH:mm:ss" format
    /// - END_TIME: End time of validity of the token in "yyyy-MM-dd HH:mm:ss" format
    /// - USERNAME: FotoWeb username of the user. Must be percent-encoded (URL-encoded)
    /// - Set w=0 foir regular login tokens and w=1 for tokens to be used with FotoWeb widgets.
    /// 
    /// For more information, see:
    /// - http://learn.fotoware.com/02_FotoWeb_8.0/Developing_with_the_FotoWeb_API/Login_Tokens
    /// - http://learn.fotoware.com/02_FotoWeb_8.0/Developing_with_the_FotoWeb_API/User_Authentication_for_Embeddable_Widgets
	/// </summary>
	public class LoginTokenGenerator
	{
		TimeSpan _vp;
        bool _useForWidgets;
		string _es;

		public LoginTokenGenerator( string encryptionSecret )
		{
			_es = encryptionSecret;
			_vp = TimeSpan.FromMinutes( 30 );
		}

		public TimeSpan ValidityPeriod
		{
			get { return _vp; }
			set { _vp = value; }
		}

        public bool UseForWidgets
        {
            get { return _useForWidgets; }
            set { _useForWidgets = value; }
        }

		public string EncryptionSecret
		{
			get { return _es; }
			set { _es = value; }
		}

        /// <summary>
        /// Creates a login token with start time set to 10 seconds in the past.
        /// This is done to account for potential time difference of Fotoweb server (which would lead to auth error).
        /// </summary>
        /// <param name="username">Username for which to generate token</param>
        /// <returns>A Fotoweb login token</returns>
		public string CreateLoginToken( string username )
		{
            DateTime startTime = DateTime.UtcNow.AddSeconds(-10);

            return CreateLoginToken(username, startTime);
		}

        public string CreateLoginToken( string username, DateTime startTime)
        {
            DateTime endTime = startTime + _vp;

            string token = String.Format("s={0};e={1};w={2};u={3};", ToUniversalDateTimeString(startTime), ToUniversalDateTimeString(endTime), _useForWidgets, username);
            token += "m=" + CreateStringMac(token) + ";";

            byte[] utf8String = StringAsUtf8ByteArray(token);
            return Convert.ToBase64String(utf8String);
        }

        public bool DecodeLoginToken( string token, out string userName, out TimeSpan remainingTime )
		{
			userName = null;
			remainingTime = TimeSpan.FromSeconds( 0 );

			string tokenData = Utf8ByteArrayToString( Convert.FromBase64String( token ) );
			string data = tokenData.Substring( 0, tokenData.LastIndexOf( ";m=" ) + 1 );
			string mac = tokenData.Substring( tokenData.LastIndexOf( ";m=" ) + 3 );
			mac = mac.Substring( 0, mac.Length - 1 );
			
			if ( mac != CreateStringMac( data ) )
				return false;

			userName = GetParamFromTokenData( data, "u" );
			DateTime startTime = ParseUniversalDateTimeString( GetParamFromTokenData( data, "s" ) );
			DateTime endTime = ParseUniversalDateTimeString( GetParamFromTokenData( data, "e" ) );
			
			if ( DateTime.UtcNow < startTime )
				return false;

			if ( DateTime.UtcNow > endTime )
				return false;

			remainingTime = endTime - DateTime.UtcNow;
			return true;
		}

		string ToUniversalDateTimeString(DateTime date)
		{
			return(date.ToString("yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture));
		}

		DateTime ParseUniversalDateTimeString(string dateString)
		{
			return(DateTime.ParseExact(dateString, "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture));
		}

		string GetParamFromTokenData( string data, string prmName )
		{
			prmName = prmName + "=";
			int startPos = data.IndexOf( prmName ) + prmName.Length;
			int endPos = data.IndexOf( ";", startPos );

			return data.Substring( startPos, endPos - startPos );
		}

		string CreateStringMac( string data )
		{
			data = data + "es=" + _es;
			byte[] dataAsUtf8 = StringAsUtf8ByteArray( data );

			MD5 hasher = new MD5CryptoServiceProvider( );
			byte[] mac = hasher.ComputeHash( dataAsUtf8 );

			return Convert.ToBase64String( mac );
		}

		byte[] StringAsUtf8ByteArray( string data )
		{
			MemoryStream ms = new MemoryStream( 100 );
			StreamWriter sw = new StreamWriter( ms, new System.Text.UTF8Encoding( false, false ) );
			sw.Write( data );
			sw.Flush();

			return ms.ToArray();
		}

		string Utf8ByteArrayToString( byte[] data )
		{
			MemoryStream ms = new MemoryStream( data );
			StreamReader sr = new StreamReader( ms, System.Text.Encoding.UTF8 );
			return sr.ReadToEnd( );
		}
	}
}
